import {
    CONNECTION_CHANGED
} from '../constants';

export function changed(event) {
    console.log('Actions', 'Connections', 'changed', event);
    return {
        type: CONNECTION_CHANGED,
        event
    };
}
