import {
    PUSH_ROUTE,
    POP_ROUTE
} from '../constants';

export function push(route) {
    return {
        type: PUSH_ROUTE,
        route
    };
};

export function pop() {
    console.log('Actions', 'Navigation', 'pop');
    return {
        type: POP_ROUTE
    };
};
