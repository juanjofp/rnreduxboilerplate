import React from 'react';
import { Provider } from 'react-redux';

import configureStore from './configureStore';
import Root from './containers/root';

const store = configureStore();

export default () => (
    <Provider
        store={ store }>
        <Root/>
    </Provider>
);
