import React from 'react';
import {
    NavigationExperimental,
    NetInfo
} from 'react-native';
import { connect } from 'react-redux';

import Header from '../../components/header';
import Home from '../../components/home';
import About from '../../components/about';
import {
    ROUTE_KEY_APP,
    ROUTE_KEY_ABOUT
} from '../../constants';
import {
    push,
    pop
} from '../../actions/navigation';
import { changed } from '../../actions/connection';

const { CardStack } = NavigationExperimental;

const Root = React.createClass({

    componentDidMount() {
        NetInfo.addEventListener(
            'change',
            this.props.handleNetInfoChanges
        );
    },

    componentWillUnmount() {
        NetInfo.removeEventListener(
            'change',
            this.props.handleNetInfoChanges
        );
    },

    renderHeader(sceneProps) {
        console.log('Root Container', 'renderHeader', sceneProps, this.props);
        return (
            <Header
                { ...sceneProps}
                back={this.props.back}/>
        );
    },

    renderScene(props) {
        console.log('Root Container', 'renderScene', props, this.props);
        let { scene } = props;
        switch (true) {
            case scene.route.key === ROUTE_KEY_APP:
                return (
                    <Home
                        navigateToAbout={this.props.navigateToAbout}/>
                );
            case scene.route.key === ROUTE_KEY_ABOUT:
                return (
                    <About />
                );
        }
    },

    render() {
        console.log('Root Container', 'render', this.state, this.props);
        return (
            <CardStack
                navigationState={ this.props.navigationState }
                renderScene={ this.renderScene }
                renderHeader={ this.renderHeader }/>
        );
    }
});

function mapStateToProps(state) {
    return {
        navigationState: state.navigation
    };
}

function mapDispatchToProps(dispatch) {
    return {
        navigateToAbout: () => {
            dispatch(push({ key: ROUTE_KEY_ABOUT, title: 'About BLEExplorer' }));
        },
        back: () => dispatch(pop()),
        handleNetInfoChanges: (evt) => {
            console.log('Root Container', 'dispatch handleNetInfoChanges', evt);
            dispatch(changed(evt));
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Root);
