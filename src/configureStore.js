import {
    createStore,
    applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';

// Reducers
import reducer from './reducers';

const configureStore = () => {
    const store = createStore(
        reducer,
        applyMiddleware(
            thunk
        )
    );

    if(module.hot) {
        module.hot.accept(
            () => {
                const nextRootReducer = require('./reducers').default;
                store.replaceReducer(nextRootReducer);
            }
        );
    }

    return store;
};

export default configureStore;
