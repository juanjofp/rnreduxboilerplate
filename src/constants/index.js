export const PUSH_ROUTE = 'PUSH_ROUTE';
export const POP_ROUTE = 'POP_ROUTE';

export const ROUTE_KEY_APP = 'ROUTE_KEY_APP';
export const ROUTE_KEY_ABOUT = 'ROUTE_KEY_ABOUT';

export const CONNECTION_CHANGED = 'CONNECTION_CHANGED';
