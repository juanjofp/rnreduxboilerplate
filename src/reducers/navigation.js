import { NavigationExperimental } from 'react-native';

import {
    PUSH_ROUTE,
    POP_ROUTE,
    ROUTE_KEY_APP
} from '../constants';

const { StateUtils } = NavigationExperimental;

const initialState = {
    index: 0,
    routes: [
        { key: ROUTE_KEY_APP, title: 'Home' }
    ]
};

const actions = {
    [PUSH_ROUTE](state, action) {
        return StateUtils.push(state, action.route);
    },
    [POP_ROUTE](state, action) {
        return StateUtils.pop(state);
    }
};

export default (state = initialState, action) => {
    console.log('Navigation Reducer', state, action);
    if(actions[action.type]) {
        return actions[action.type](state, action);
    }
    return state;
};
