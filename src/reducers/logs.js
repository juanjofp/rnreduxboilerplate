export default (state = { actions: [] }, action) => {
    console.log('Logs Reducer', state, action);
    return {
        actions: [
            ...state.actions,
            action
        ]
    };
};
