import { combineReducers } from 'redux';

import logs from './logs';
import navigation from './navigation';

export default combineReducers({
    logs,
    navigation
});
