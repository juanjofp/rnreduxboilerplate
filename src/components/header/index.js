import React from 'react';
import { NavigationExperimental } from 'react-native';

const { Header } = NavigationExperimental;

const renderTitle = (props) => {
    console.log('Header', 'renderTitle', props);
    return (
        <Header.Title>
            {props.scene.route.title}
        </Header.Title>
    );
};

export default ({
    back,
    ...props
}) => {
    console.log('Header', props);
    return (
        <Header
            { ...props }
            renderTitleComponent={ renderTitle }
            onNavigateBack={ back }/>
    );
};
