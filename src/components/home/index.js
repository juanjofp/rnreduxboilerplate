import React from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';

import styles from './styles';

export default ({
    navigateToAbout
}) => {
    console.log('Home');
    return (
        <View
            style={ styles.container }>
            <Text>BLE Explorer</Text>
            <Button
                onPress={navigateToAbout}
                title="Learn More"
                color="#841584"
                accessibilityLabel="Learn more about BLEExplorer"
                />
        </View>
    );
};
