import React from 'react';
import {
    View,
    Text
} from 'react-native';
import styles from './styles';

export default () => {
    console.log('About');
    return (
        <View
            style={ styles.container }>
            <Text>
                This app show all BLE Devices near to me
            </Text>
        </View>
    );
};
